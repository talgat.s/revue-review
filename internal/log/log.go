package log

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/talgat.s/revue-review/config"
	"gitlab.com/talgat.s/revue-review/internal/constant"
)

func Conf(conf *config.Config) {
	if conf.Env != constant.EnvironmentLocal {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
}
