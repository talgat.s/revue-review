package router

import "github.com/gofiber/fiber/v2"

// hello hanlde api status
func (r *Router) ping(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"message": "pong"})
}
