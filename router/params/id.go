package params

type ID64 struct {
	ID int64 `params:"id"`
}

type IDStr struct {
	ID string `params:"id"`
}
