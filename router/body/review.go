package body

type ReviewPost struct {
	ProductID *string `json:"productId,omitempty" xml:"productId,omitempty" form:"productId,omitempty" validate:"required"`
	Score     *int64  `json:"score,omitempty" xml:"score,omitempty" form:"score,omitempty" validate:"required,number,min=1,max=5"`
}

type ReviewPatch struct {
	ProductID *string `json:"productId,omitempty" xml:"productId,omitempty" form:"productId,omitempty"`
	Score     *int64  `json:"score,omitempty" xml:"score,omitempty" form:"score,omitempty" validate:"omitempty,number,min=1,max=5"`
}
