package router

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/talgat.s/revue-review/router/body"
	"gitlab.com/talgat.s/revue-review/router/params"
	"gitlab.com/talgat.s/revue-review/router/query"
	"gitlab.com/talgat.s/revue-review/utils/validate"
)

func (r *Router) reviewGroup(app fiber.Router) {
	rvw := app.Group("/review")

	rvw.Get("/:id", r.getReview)
	rvw.Get("/", r.getReviews)
	rvw.Post("/", r.postReview)
	rvw.Patch("/:id", r.patchReview)
	rvw.Delete("/:id", r.deleteReview)
}

func (r *Router) getReview(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.ID64{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	// db
	if rvw, err := r.DB.SelectReview(ctx, p.ID); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(rvw)
	}
}

func (r *Router) getReviews(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// query
	q := new(query.Pagination)
	if err := c.QueryParser(q); err != nil {
		return err
	}
	if err := validate.Struct(q); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err)
	}

	// db
	if rvws, err := r.DB.SelectReviews(ctx, q.Offset, q.Limit); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(rvws)
	}
}

func (r *Router) postReview(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// body
	b := new(body.ReviewPost)
	if err := c.BodyParser(b); err != nil {
		return err
	}
	if err := validate.Struct(b); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err)
	}

	// db
	if rvw, err := r.DB.CreateReview(ctx, b.ProductID, b.Score); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(rvw)
	}
}

func (r *Router) patchReview(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.ID64{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	// body
	b := new(body.ReviewPatch)
	if err := c.BodyParser(b); err != nil {
		return err
	}
	if err := validate.Struct(b); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err)
	}

	// db
	if rvw, err := r.DB.UpdateReview(ctx, p.ID, b.ProductID, b.Score); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(rvw)
	}
}

func (r *Router) deleteReview(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.ID64{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	// db
	if err := r.DB.DeleteReview(ctx, p.ID); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(c.JSON(fiber.Map{"success": true}))
	}
}
