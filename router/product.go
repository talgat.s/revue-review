package router

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/talgat.s/revue-review/router/body"
	"gitlab.com/talgat.s/revue-review/router/params"
	"gitlab.com/talgat.s/revue-review/router/query"
	"gitlab.com/talgat.s/revue-review/utils/validate"
)

func (r *Router) productGroup(app fiber.Router) {
	rvw := app.Group("/product")

	rvw.Get("/:id", r.getProduct)
	rvw.Get("/", r.getProducts)
	rvw.Post("/", r.postProduct)
	rvw.Delete("/:id", r.deleteProduct)
}

func (r *Router) getProduct(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.IDStr{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	// db
	if prd, err := r.DB.SelectProduct(ctx, p.ID); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(prd)
	}
}

func (r *Router) getProducts(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// query
	p := new(query.Pagination)
	if err := c.QueryParser(p); err != nil {
		return err
	}
	if errors := validate.Struct(p); errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors)
	}

	// db
	if prds, err := r.DB.SelectProducts(ctx, p.Offset, p.Limit); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(prds)
	}
}

func (r *Router) postProduct(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// body
	b := new(body.ProductPost)
	if err := c.BodyParser(b); err != nil {
		return err
	}
	if err := validate.Struct(b); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err)
	}

	// db
	if prd, err := r.DB.CreateProduct(ctx, *b.ID); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(prd)
	}
}

func (r *Router) deleteProduct(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.IDStr{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	// db
	if err := r.DB.DeleteProduct(ctx, p.ID); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	} else {
		return c.JSON(c.JSON(fiber.Map{"success": true}))
	}
}
