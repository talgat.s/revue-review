package router

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"

	"gitlab.com/talgat.s/revue-review/cmd/db"
)

type Router struct {
	DB *db.DB
}

func (r *Router) Setup(ctx context.Context, app *fiber.App) {
	// Match any request
	app.Use(func(c *fiber.Ctx) error {
		c.SetUserContext(ctx)
		return c.Next()
	})

	api := app.Group("/", logger.New())
	api.Get("/ping", r.ping)

	// review
	r.reviewGroup(api)

	r.productGroup(api)

	// 	// Auth
	// 	auth := api.Group("/auth")
	// 	auth.Post("/login", handler.Login)
	//
	// 	// User
	// 	user := api.Group("/user")
	// 	user.Get("/:id", handler.GetUser)
	// 	user.Post("/", handler.CreateUser)
	// 	user.Patch("/:id", middleware.Protected(), handler.UpdateUser)
	// 	user.Delete("/:id", middleware.Protected(), handler.DeleteUser)
	//
	// 	// Product
	// 	product := api.Group("/product")
	// 	product.Get("/", handler.GetAllProducts)
	// 	product.Get("/:id", handler.GetProduct)
	// 	product.Post("/", middleware.Protected(), handler.CreateProduct)
	// 	product.Delete("/:id", middleware.Protected(), handler.DeleteProduct)
}
