package query

type Pagination struct {
	Limit  int `query:"limit" validate:"omitempty,number,min=1"`
	Offset int `query:"offset" validate:"omitempty,number,min=0"`
}
