FROM golang:1.19-alpine3.16 AS builder
WORKDIR /app
COPY . .
RUN go build -o api .

FROM alpine:3.16 AS runner

# Add Maintainer Info
LABEL maintainer="Talgat Saribayev <talgat.s@protonmail.com>"

WORKDIR /app
COPY --from=builder /app/api api
EXPOSE 80
CMD ["/app/api"]

