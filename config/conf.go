package config

import (
	"context"
	"flag"

	"gitlab.com/talgat.s/revue-review/internal/constant"
)

type SharedConfig struct {
	Port int    `env:"PORT,default=80"`
	Host string `env:"HOST"`
}

type Config struct {
	Env constant.Environment
	DB  *DBConfig
	Api *ApiConfig
}

func NewConfig(ctx context.Context) (*Config, error) {
	conf := &Config{
		Env: getEnv(),
	}

	_ = conf.loadDotEnvFiles()

	// DB config
	if c, err := NewDBConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.DB = c
	}

	// Api config
	if c, err := NewApiConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Api = c
	}

	flag.Parse()

	return conf, nil
}
