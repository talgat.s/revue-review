package config

import (
	"context"
	"flag"

	"github.com/sethvargo/go-envconfig"
)

type DBConfig struct {
	*SharedConfig `env:",prefix=DB_"`
	Name          string `env:"DB_NAME"`
	User          string `env:"DB_USER"`
	Password      string `env:"DB_PASSWORD"`
}

func NewDBConfig(ctx context.Context) (*DBConfig, error) {
	c := &DBConfig{}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.StringVar(&c.Host, "db-host", c.Host, "db host [DB_HOST]")
	flag.IntVar(&c.Port, "db-port", c.Port, "db port [DB_PORT]")
	flag.StringVar(&c.Name, "db-name", c.Name, "db name [DB_NAME]")
	flag.StringVar(&c.User, "db-user", c.User, "db user [DB_USER]")
	flag.StringVar(&c.Password, "db-password", c.Password, "db password [DB_PASSWORD]")

	return c, nil
}
