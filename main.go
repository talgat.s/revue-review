package main

import (
	"context"

	"github.com/sirupsen/logrus"

	"gitlab.com/talgat.s/revue-review/cmd/api"
	"gitlab.com/talgat.s/revue-review/cmd/db"
	configs "gitlab.com/talgat.s/revue-review/config"
	"gitlab.com/talgat.s/revue-review/internal/constant"
	llog "gitlab.com/talgat.s/revue-review/internal/log"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// conf
	conf, err := configs.NewConfig(ctx)
	if err != nil {
		logrus.Panic(err)
	}

	// setup logger
	llog.Conf(conf)

	// configure db service
	db := &db.DB{
		Log:  logrus.WithField("service", constant.DB),
		Conf: conf.DB,
	}
	if err := db.Setup(ctx); err != nil {
		logrus.Panic("db setup", err)
	}

	// configure gateway service
	a := &api.Api{
		Log:  logrus.WithField("service", constant.Api),
		Conf: conf.Api,
		DB:   db,
	}
	// start gateway service
	a.StartServer(ctx, cancel)

	<-ctx.Done()
	// Your cleanup tasks go here
	logrus.Info("cleaning up ...")
	db.Close(ctx)

	logrus.Info("server was successful shutdown.")
}
