package seed

import (
	"context"
	"os"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dbfixture"

	"gitlab.com/talgat.s/revue-review/cmd/db/model"
)

func Seed(ctx context.Context, db *bun.DB) error {
	db.RegisterModel((*model.Product)(nil))
	db.RegisterModel((*model.Review)(nil))

	fixture := dbfixture.New(db)
	return fixture.Load(ctx, os.DirFS("."), "fixture.yml")
}
