package db

import (
	"context"

	"gitlab.com/talgat.s/revue-review/cmd/db/model"
)

func (d *DB) SelectProduct(ctx context.Context, id string) (*model.ProductCount, error) {
	product := &model.ProductCount{
		ID: id,
	}

	if err := d.client.
		NewSelect().
		Model(product).
		WherePK().
		TableExpr("products AS p").
		Column("p.id").
		ColumnExpr("COUNT(p.id) AS reviews_count").
		ColumnExpr("AVG(r.score) AS reviews_avg").
		Join("JOIN reviews AS r ON p.id = r.product_id").
		Group("p.id").
		Scan(ctx); err != nil {
		return nil, err
	}

	return product, nil
}

func (d *DB) SelectProducts(ctx context.Context, offset, limit int) ([]model.ProductCount, error) {
	var products []model.ProductCount

	if err := d.client.
		NewSelect().
		TableExpr("products AS p").
		Column("p.id").
		ColumnExpr("COUNT(p.id) AS reviews_count").
		ColumnExpr("AVG(r.score) AS reviews_avg").
		Join("JOIN reviews AS r ON p.id = r.product_id").
		Group("p.id").
		Limit(limit).
		Offset(offset).
		Scan(ctx, &products); err != nil {
		return nil, err
	}

	return products, nil
}

func (d *DB) CreateProduct(ctx context.Context, id string) (*model.ProductCount, error) {
	product := &model.Product{
		ID: id,
	}

	if res, err := d.client.
		NewInsert().
		Model(product).
		Exec(ctx); err != nil {
		return nil, err
	} else if _, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return d.SelectProduct(ctx, id)
	}
}

func (d *DB) DeleteProduct(ctx context.Context, id string) error {
	product := &model.Product{
		ID: id,
	}

	if _, err := d.client.
		NewDelete().
		Model(product).
		WherePK().
		Exec(ctx); err != nil {
		return err
	}
	return nil
}
