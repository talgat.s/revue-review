package model

import (
	"time"

	"github.com/uptrace/bun"
)

type Review struct {
	bun.BaseModel `bun:"table:reviews"`

	ID        int64      `bun:"id,pk,autoincrement" json:"id"`
	ProductID *string    `bun:"product_id" json:"productId"`
	Product   *Product   `bun:"rel:belongs-to,join:product_id=id" json:",omitempty"`
	Score     int64      `bun:"score,type:integer,notnull,default:0" json:"score"`
	CreatedAt *time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"createdAt"`
	UpdatedAt *time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"updatedAt"`
}
