package model

import (
	"time"

	"github.com/uptrace/bun"
)

type Product struct {
	bun.BaseModel `bun:"table:products"`

	ID        string     `bun:"id,pk" json:"id"`
	Reviews   []*Review  `bun:"rel:has-many,join:id=product_id" json:",omitempty"`
	CreatedAt *time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"createdAt"`
	UpdatedAt *time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"updatedAt"`
}

type ProductCount struct {
	bun.BaseModel `bun:"table:products"`

	ID           string  `bun:"id,pk" json:"id"`
	ReviewsCount int     `bun:"reviews_count" json:"reviewsCount"`
	ReviewsAvg   float32 `bun:"reviews_avg" json:"reviewsAvg"`
}
