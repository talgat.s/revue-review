package db

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"

	configs "gitlab.com/talgat.s/revue-review/config"
)

type DB struct {
	Log    *logrus.Entry
	Conf   *configs.DBConfig
	client *bun.DB
}

func GetClient(conf *configs.DBConfig) *bun.DB {
	addr := fmt.Sprintf("%s:%d", conf.Host, conf.Port)

	pgconn := pgdriver.NewConnector(
		pgdriver.WithAddr(addr),
		pgdriver.WithUser(conf.User),
		pgdriver.WithPassword(conf.Password),
		pgdriver.WithDatabase(conf.Name),
		pgdriver.WithTLSConfig(nil),
		pgdriver.WithApplicationName("revue"),
	)
	sqldb := sql.OpenDB(pgconn)

	return bun.NewDB(sqldb, pgdialect.New())
}

func (d *DB) Setup(ctx context.Context) error {
	d.client = GetClient(d.Conf)

	// if err := d.defineModels(ctx); err != nil {
	// 	d.Log.Error("fail db define model", err)
	// 	return err
	// }

	return nil
}

func (d *DB) Close(ctx context.Context) error {
	// if err := d.dropModels(ctx); err != nil {
	// 	d.Log.Error("fail db drop model", err)
	// 	return err
	// }

	if err := d.client.Close(); err != nil {
		d.Log.Error("fail db client close", err)
		return err
	}

	return nil
}
