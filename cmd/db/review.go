package db

import (
	"context"

	"gitlab.com/talgat.s/revue-review/cmd/db/model"
)

func (d *DB) SelectReview(ctx context.Context, id int64) (*model.Review, error) {
	review := &model.Review{
		ID: id,
	}

	if err := d.client.
		NewSelect().
		Model(review).
		WherePK().
		Scan(ctx); err != nil {
		return nil, err
	}

	return review, nil
}

func (d *DB) SelectReviews(ctx context.Context, offset, limit int) ([]model.Review, error) {
	var reviews []model.Review

	if err := d.client.
		NewSelect().
		Model(&reviews).
		Limit(limit).
		Offset(offset).
		Scan(ctx); err != nil {
		return nil, err
	}

	return reviews, nil
}

func (d *DB) CreateReview(ctx context.Context, productId *string, score *int64) (*model.Review, error) {
	review := &model.Review{
		ProductID: productId,
		Score:     *score,
	}

	if res, err := d.client.
		NewInsert().
		Model(review).
		Exec(ctx); err != nil {
		return nil, err
	} else if id, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return d.SelectReview(ctx, id)
	}
}

func (d *DB) UpdateReview(ctx context.Context, id int64, productId *string, score *int64) (*model.Review, error) {
	review := &model.Review{
		ID: id,
	}

	if productId != nil {
		review.ProductID = productId
	}
	if score != nil {
		review.Score = *score
	}

	if res, err := d.client.
		NewUpdate().
		Model(review).
		WherePK().
		Exec(ctx); err != nil {
		return nil, err
	} else if id, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return d.SelectReview(ctx, id)
	}
}

func (d *DB) DeleteReview(ctx context.Context, id int64) error {
	review := &model.Review{
		ID: id,
	}

	if _, err := d.client.
		NewDelete().
		Model(review).
		WherePK().
		Exec(ctx); err != nil {
		return err
	}
	return nil
}
