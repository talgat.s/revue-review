package migrations

import (
	"context"

	"github.com/uptrace/bun"

	"gitlab.com/talgat.s/revue-review/cmd/db/model"
)

func init() {
	Migrations.MustRegister(func(ctx context.Context, db *bun.DB) error {
		if _, err := db.NewCreateTable().Model(&model.Product{}).Exec(ctx); err != nil {
			return err
		}
		return nil
	}, func(ctx context.Context, db *bun.DB) error {
		if _, err := db.NewDropTable().Model(&model.Product{}).Exec(ctx); err != nil {
			return err
		}
		return nil
	})
}
